package com.bstek.ureport.export.html;

public class TextInputComponentData extends InputComponentData {

	private Object value;

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
